#!/usr/bin/env python3
import sys

def main(argv):
    sequence = argv[0]
    print(f"Observation sequence Q: {sequence}")
    print(f'Length of Q: {len(sequence)}\n')

    # (Prior, Prob cherry, Prob Lime)
    bags = [(.1, 1, 0), (.2, .75, .25), (.4, .5, .5), (.2, .25, .75), (.1,0,1)]
    probs = []

    for candy in sequence:
        if candy != 'L' and candy !=  'C':
            print("INVALID INPUT!")
            exit(-1)

    for bag in bags:

        # Get P(Q | BN)
        q_given_b = prob_sequence_given_bag(sequence, bag)

        # Get P(Q)
        probQ = prob_sequence(sequence, bags)

        final = (q_given_b * bag[0]) / probQ
        
        # Add the Final Prob to the list
        probs.extend([final])

    # Print Final Results
    for i in range(len(probs)):
        print(f"P(h{i} | {sequence}) = {probs[i]}")

def prob_sequence_given_bag(sequence, current_bag):
    final = 1
    for candy in sequence:
        if candy == "C":
            final *= current_bag[1]
        else:
            final *= current_bag[2]

    return final

def prob_sequence(sequence, bags):
    final = 0
    for bag in bags:
        final += prob_sequence_given_bag(sequence, bag) * bag[0]
    
    return final

if __name__ == "__main__":
    main(sys.argv[1:])